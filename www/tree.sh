#!/bin/sh

echo "Content-type: text/plain\n";

PIN=21

if [ "$SCRIPT_NAME" = /tree-on ]; then
    gpioset gpiochip0 $PIN=1
else
    gpioset gpiochip0 $PIN=0
fi
