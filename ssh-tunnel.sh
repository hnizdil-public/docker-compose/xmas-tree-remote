#!/bin/sh

# Makes a tunnel from $REMOTE:$PORT to localhost:80
# Note that 172.17.0.1 is the gateway between the Docker host and the bridge network

REMOTE=$1
PORT=29381

autossh \
	-M 0 \
	-o "ExitOnForwardFailure yes" \
	-o "ServerAliveInterval 30" \
	-o "ServerAliveCountMax 3" \
	-T \
	-N \
	-R "172.17.0.1:${PORT}:localhost:80" \
	"$REMOTE"
