FROM nginx:1.21

RUN apt-get update \
    && apt-get install -y \
        spawn-fcgi \
        fcgiwrap \
        # Look for setup-libgpiod.sh
        libgpiod-dev \
        gpiod \
        python3-libgpiod \
        libgpiod-doc

RUN addgroup --system --gid 993 gpio
RUN usermod -a -G gpio nginx

RUN chown nginx:nginx /etc/init.d/fcgiwrap

RUN sed -i \
    -e 's/FCGI_USER="www-data"/FCGI_USER="nginx"/g' \
    -e 's/FCGI_GROUP="www-data"/FCGI_GROUP="997"/g' \
    -e 's/FCGI_SOCKET_OWNER="www-data"/FCGI_SOCKET_OWNER="nginx"/g' \
    -e 's/FCGI_SOCKET_GROUP="www-data"/FCGI_SOCKET_GROUP="nginx"/g' \
    /etc/init.d/fcgiwrap

COPY ./vhost.conf /etc/nginx/conf.d/default.conf

WORKDIR /var/www

CMD /etc/init.d/fcgiwrap start \
    && nginx -g 'daemon off;'
